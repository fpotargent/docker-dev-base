FROM ubuntu:xenial

ENV UPDATED_ON 2017-04-23 20:00
ENV DEBIAN_FRONTEND noninteractive

# Prepare apt for installing software
RUN apt-get update && apt-get install -y \
      apt-file \
      apt-utils \
      apt-transport-https \
      curl \
      sudo

# Add extra PPAs: git
COPY apt-extra.list /etc/apt/sources.list.d/
# Fetch git ppa key
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E1DF1F24

RUN apt-get update
RUN apt-file update

# Create and configure user
COPY skel/ /etc/skel/
RUN adduser me --gecos "" --disabled-password && \
    echo "me ALL=(ALL:ALL) NOPASSWD:ALL" >/etc/sudoers.d/me && \
    chmod 0440 /etc/sudoers.d/me

# Setup container start
RUN echo dev >/etc/debian_chroot

COPY init.sh /init.sh
CMD /init.sh

# Install software
RUN apt-get install -y \
      bash-completion \
      less \
      p7zip-full \
      udev \
      wajig

# Basic dev tools
RUN apt-get install -y \
      autoconf \
      automake \
      autopoint \
      bison \
      build-essential \
      byacc \
      flex \
      libtool

# Who can live without git?
RUN apt-get install -y git-core git-flow git-gui gitk
