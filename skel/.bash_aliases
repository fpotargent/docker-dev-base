export VISUAL=nano

alias ll='ls -lF'
alias e=emacs
alias more=less

alias tree='tree -A'
