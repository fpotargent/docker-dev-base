#!/bin/bash
if [[ "x`id -g me`" != "x$MYGID" ]]; then
    groupmod --gid $MYGID me
fi
if [[ "x`id -u me`" != "x$MYUID" ]]; then
    usermod -u $MYUID me
fi
exec su -l me
